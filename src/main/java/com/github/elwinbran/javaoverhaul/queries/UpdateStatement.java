/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.queries;

import com.github.elwinbran.javaoverhaul.containers.ContainerDecoration;
import com.github.elwinbran.javaoverhaul.containers.Table;
import com.github.elwinbran.javaoverhaul.peo.SerializableComposite;
import java.util.function.Function;


/**
 * A full statement to be executed of a set of stored objects.
 * The statement checks if a entry/record/file is affected first and then 
 * updates it according to the implementation.
 * 
 * @author Elwin Slokker
 * 
 * @param <PersistableT> Type of object that needs to be updated.
 */
public interface UpdateStatement<PersistableT>
{
    /**
     * Should treat PersistableT's as immutables (obviously).
     * TODO: useless method maybe.
     * 
     * @param persistable
     * @return 
     */
    public abstract PersistableT update(PersistableT persistable);
    
    public ContainerDecoration<PersistableT> update(
            final Iterable<PersistableT> oldItems);
    
    public abstract boolean isIndexBased();
    
    public abstract Iterable<SerializableComposite> getTargetIndices();
    
    public abstract Table<String, Function<Object, Object>> getUpdate();
}
